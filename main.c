/*
 * This file is part of armexec.
 *
 * armexec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * armexec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with armexec.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <stdint.h>

#include "armexec.h"

int
main(int argc, char **argv)
{
    FILE *fp;
    struct armld ld;
    struct elf *elf;
    struct vm *vm;
    int ret;
    uint32_t args[4] = {0};

    if (!(fp = fopen(argv[1], "r"))) {
        printf("Cannot open file [%s]\n", argv[1]);
        return -1;
    }

    vm = vm_init();
    elf = elf_load(vm, fp);
    vm_set_elf(vm, elf);
    fclose(fp);

    ld.vm = vm;
    ld.elf = elf;

    setbuf(stdout,NULL);
    setbuf(stderr,NULL);

    //exec_set_breakpoint(0xc62);
    //vm_set_mem_watch(0x79490, 1);

    args[0] = 1;
    args[1] = 2;

    ret = arm_exec(&ld, 0xc60, 0, args, 4);
    printf("return: %d\n", ret);

    return 0;
}
