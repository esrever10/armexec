/*
 * This file is part of armexec.
 *
 * armexec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * armexec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with armexec.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <string.h>
#include <stdint.h>

#include "armexec.h"
#include "plt_stub.h"

DCLR_STUB(memset)
{
    struct vm *vm = ld->vm;
    void *s = PA(r0);

    memset(s, r1, r2);

    return 0;
}

DCLR_STUB(memcpy)
{
    struct vm *vm = ld->vm;
    char *dst = PA(r0);
    char *src = PA(r1);
    uint32_t len = r2;

    memcpy(dst, src, len);

    return r0;
}

DCLR_STUB(memmove)
{
    struct vm *vm = ld->vm;
    char *dst = PA(r0);
    char *src = PA(r1);
    uint32_t len = r2;

    memmove(dst, src, len);

    return r0;
}

DCLR_STUB(strcpy)
{
    struct vm *vm = ld->vm;
    char *dst = PA(r0);
    char *src = PA(r1);

    strcpy(dst, src);

    return r0;
}

DCLR_STUB(strstr)
{
    struct vm *vm = ld->vm;
    char *s1 = PA(r0);
    char *s2 = PA(r1);
    char *p;

    p = strstr(s1, s2);

    if (!p)
        return 0;

    return r0 + (p - s1);
}

DCLR_STUB(strncpy)
{
    struct vm *vm = ld->vm;
    char *dst = PA(r0);
    char *src = PA(r1);
    uint32_t len = r2;

    strncpy(dst, src, len);

    return r0;
}

DCLR_STUB(strcat)
{
    struct vm *vm = ld->vm;
    void *dst = PA(r0);
    void *src = PA(r1);

    strcat(dst, src);

    return r0;
}

DCLR_STUB(strncat)
{
    struct vm *vm = ld->vm;
    void *dst = PA(r0);
    void *src = PA(r1);
    uint32_t len = r2;

    strncat(dst, src, len);

    return r0;
}

DCLR_STUB(strlen)
{
    struct vm *vm = ld->vm;
    void *s = PA(r0);

    return strlen(s);
}

DCLR_STUB(strdup)
{
    struct vm *vm = ld->vm;
    char *s = PA(r0);
    int len;
    uint32_t ret;
    void *p;

    len = strlen(s);
    ret = vm_malloc(vm, len + 1, &p);
    strcpy(p, s);

    return ret;
}

DCLR_STUB(strcmp)
{
    struct vm *vm = ld->vm;
    void *s1 = PA(r0);
    void *s2 = PA(r1);

    return strcmp(s1, s2);
}

DCLR_STUB(strcasecmp)
{
    struct vm *vm = ld->vm;
    void *s1 = PA(r0);
    void *s2 = PA(r1);

    return strcasecmp(s1, s2);
}

DCLR_STUB(memcmp)
{
    struct vm *vm = ld->vm;
    void *s1 = PA(r0);
    void *s2 = PA(r1);

    return memcmp(s1, s2, r2);
}

DCLR_STUB(strncmp)
{
    struct vm *vm = ld->vm;
    void *s1 = PA(r0);
    void *s2 = PA(r1);
    uint32_t len = r2;

    return strncmp(s1, s2, len);
}

DCLR_STUB(strchr)
{
    struct vm *vm = ld->vm;
    char *s = PA(r0);
    int c = r1;
    char *ret;

    ret = strchr(s, c);

    if (ret == NULL)
        return 0;

    return r0 + (ret - s);
}

DCLR_STUB(memchr)
{
    struct vm *vm = ld->vm;
    char *s = PA(r0);
    int c = r1;
    int n = r2;
    char *ret;

    ret = memchr(s, c, n);

    if (ret == NULL)
        return 0;

    return r0 + (ret - s);
}

DCLR_STUB(strncasecmp)
{
    struct vm *vm = ld->vm;
    char *s1 = PA(r0);
    char *s2 = PA(r1);
    uint32_t n = r2;

    return strncasecmp(s1, s2, n);
}

DCLR_STUB(strrchr)
{
    struct vm *vm = ld->vm;
    char *s = PA(r0);
    int c = r1;
    char *ret;

    ret = strrchr(s, c);

    if (ret == NULL)
        return 0;

    return r0 + (ret - s);
}
